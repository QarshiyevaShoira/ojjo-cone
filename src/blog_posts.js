export default [
    {
        id: 'b1',
        title: 'HOW TO CHOOSE GIFTS FOR WOMEN IN 6 STEPS',
        text: `Like so many other products, jewellery sales are increasingly moving online rather than taking place in a traditional brick and mortar store. Shopping online is of course quick and convenient, plus it allows you to shop from places far away from your location.

        While being able to see a piece of jewellery and trying it on in person is great, the downside is the inconvenience of needing to travel to a store and being limited to what that particular store has available. You'll also miss out on beautiful artisan jewellery created by independent designers who generally don't have bricks and mortar stores (like us!).`,
        img: 'https://cdn.shopify.com/s/files/1/0021/6592/articles/how-to-choose-great-gifts-for-women-in-6-steps-574472_400x.jpg?v=1661354772',
    },
    {
        id: 'b2',
        title: 'TOP 10 TRENDS IN 2022 FOR JEWELLERY IN AUSTRALIA',
        text: `Like so many other products, jewellery sales are increasingly moving online rather than taking place in a traditional brick and mortar store. Shopping online is of course quick and convenient, plus it allows you to shop from places far away from your location.

        While being able to see a piece of jewellery and trying it on in person is great, the downside is the inconvenience of needing to travel to a store and being limited to what that particular store has available. You'll also miss out on beautiful artisan jewellery created by independent designers who generally don't have bricks and mortar stores (like us!).`,
        img: 'https://cdn.shopify.com/s/files/1/0021/6592/articles/top-10-trends-in-2022-for-jewellery-in-australia-568212_400x.jpg?v=1661354759',
    },
    {
        id: 'b3',
        title: '7 ONLINE JEWELLERY SHOPPING MISTAKES TO AVOID',
        text: `Like so many other products, jewellery sales are increasingly moving online rather than taking place in a traditional brick and mortar store. Shopping online is of course quick and convenient, plus it allows you to shop from places far away from your location.

        While being able to see a piece of jewellery and trying it on in person is great, the downside is the inconvenience of needing to travel to a store and being limited to what that particular store has available. You'll also miss out on beautiful artisan jewellery created by independent designers who generally don't have bricks and mortar stores (like us!).`,
        img: 'https://cdn.shopify.com/s/files/1/0021/6592/articles/7-online-jewellery-shopping-mistakes-to-avoid-953961_400x.jpg?v=1661354754',
    },
    {
        id: 'b4',
        title: "A JEWELLERY LOVER'S GUIDE TO BUILDING THE PERFECT RING STACK",
        text: `Like so many other products, jewellery sales are increasingly moving online rather than taking place in a traditional brick and mortar store. Shopping online is of course quick and convenient, plus it allows you to shop from places far away from your location.

        While being able to see a piece of jewellery and trying it on in person is great, the downside is the inconvenience of needing to travel to a store and being limited to what that particular store has available. You'll also miss out on beautiful artisan jewellery created by independent designers who generally don't have bricks and mortar stores (like us!).`,
        img: 'https://cdn.shopify.com/s/files/1/0021/6592/articles/a-jewellery-lovers-guide-to-building-the-perfect-ring-stack-977798_400x.jpg?v=1661354755',
    },
    {
        id: 'b5',
        title: 'HOW TO WEAR DIFFERENT TYPES OF EARRINGS',
        text: `Like so many other products, jewellery sales are increasingly moving online rather than taking place in a traditional brick and mortar store. Shopping online is of course quick and convenient, plus it allows you to shop from places far away from your location.

        While being able to see a piece of jewellery and trying it on in person is great, the downside is the inconvenience of needing to travel to a store and being limited to what that particular store has available. You'll also miss out on beautiful artisan jewellery created by independent designers who generally don't have bricks and mortar stores (like us!).`,
        img: 'https://cdn.shopify.com/s/files/1/0021/6592/articles/how-to-wear-different-types-of-earrings-565646_400x.jpg?v=1661354754',
    },
    {
        id: 'b6',
        title: '9 WINTER JEWELLERY TRENDS IN AUSTRALIA IN 2022',
        text: `Like so many other products, jewellery sales are increasingly moving online rather than taking place in a traditional brick and mortar store. Shopping online is of course quick and convenient, plus it allows you to shop from places far away from your location.

        While being able to see a piece of jewellery and trying it on in person is great, the downside is the inconvenience of needing to travel to a store and being limited to what that particular store has available. You'll also miss out on beautiful artisan jewellery created by independent designers who generally don't have bricks and mortar stores (like us!).`,
        img: 'https://cdn.shopify.com/s/files/1/0021/6592/articles/9-winter-jewellery-trends-in-australia-in-2022-962962_400x.jpg?v=1661354755',
    },
    {
        id: 'b7',
        title: '6 TIMELESS JEWELLERY GIFTS FOR HER THAT NEVER GO OUT OF STYLE',
        text: `Like so many other products, jewellery sales are increasingly moving online rather than taking place in a traditional brick and mortar store. Shopping online is of course quick and convenient, plus it allows you to shop from places far away from your location.

        While being able to see a piece of jewellery and trying it on in person is great, the downside is the inconvenience of needing to travel to a store and being limited to what that particular store has available. You'll also miss out on beautiful artisan jewellery created by independent designers who generally don't have bricks and mortar stores (like us!).`,
        img: 'https://cdn.shopify.com/s/files/1/0021/6592/articles/6-timeless-jewellery-gifts-for-her-that-never-go-out-of-style-478334_400x.jpg?v=1663777511',
    },
    {
        id: 'b8',
        title: 'STUNNING AUSTRALIAN RINGS FOR WOMEN (AND WHERE TO FIND THEM)',
        text: `Like so many other products, jewellery sales are increasingly moving online rather than taking place in a traditional brick and mortar store. Shopping online is of course quick and convenient, plus it allows you to shop from places far away from your location.

        While being able to see a piece of jewellery and trying it on in person is great, the downside is the inconvenience of needing to travel to a store and being limited to what that particular store has available. You'll also miss out on beautiful artisan jewellery created by independent designers who generally don't have bricks and mortar stores (like us!).`,
        img: 'https://cdn.shopify.com/s/files/1/0021/6592/articles/stunning-australian-rings-for-women-and-where-to-find-them-107983_400x.jpg?v=1661354753',
    },
    {
        id: 'b9',
        title: '5 TIPS TO FIND THE BEST ONLINE JEWELLERY STORE',
        text: `Like so many other products, jewellery sales are increasingly moving online rather than taking place in a traditional brick and mortar store. Shopping online is of course quick and convenient, plus it allows you to shop from places far away from your location.

        While being able to see a piece of jewellery and trying it on in person is great, the downside is the inconvenience of needing to travel to a store and being limited to what that particular store has available. You'll also miss out on beautiful artisan jewellery created by independent designers who generally don't have bricks and mortar stores (like us!).`,
        img: 'https://cdn.shopify.com/s/files/1/0021/6592/files/find-best-online-jewellery-store.jpg?v=1658313329',
    },
]