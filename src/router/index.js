import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '@/views/Home/Home.vue'
import Blog from '@/views/Blog/Blog.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home
  },
  {
    path: '/blog',
    name: 'Blog',
    component: Blog, 
  },
  {
    path: '/post/:postId',
    name: 'Post',
    component: () => import('@/views/Blog/components/BlogPosts.vue'), 
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
